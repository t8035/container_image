ARG BASE_IMAGE
FROM ${BASE_IMAGE}

ARG TF_VERSION
ARG TF_ARCH

RUN apk add jq

RUN curl -s -LO https://releases.hashicorp.com/terraform/$TF_VERSION/terraform_${TF_VERSION}_${TF_ARCH}.zip && \
    unzip -o terraform_${TF_VERSION}_${TF_ARCH}.zip terraform && \
    cp terraform /usr/bin && \
    rm terraform_${TF_VERSION}_${TF_ARCH}.zip terraform

USER cloudsdk
WORKDIR /home/cloudsdk
